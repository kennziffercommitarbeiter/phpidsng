plugin.tx_phpidsng {
	view {
		# cat=plugin.tx_phpidsng/file; type=string; label=Path to template root (FE)
		templateRootPath = EXT:phpidsng/Resources/Private/Templates/
		# cat=plugin.tx_phpidsng/file; type=string; label=Path to template partials (FE)
		partialRootPath = EXT:phpidsng/Resources/Private/Partials/
		# cat=plugin.tx_phpidsng/file; type=string; label=Path to template layouts (FE)
		layoutRootPath = EXT:phpidsng/Resources/Private/Layouts/
	}
	persistence {
		# cat=plugin.tx_phpidsng//a; type=string; label=Default storage PID
		storagePid =
	}
	settings {
	    # cat=plugin.tx_phpidsng/enable/001; type=boolean; label=Debug Mode: Turn on or off debug mode.
        General.debug_mode     	= 0

        # cat=plugin.tx_phpidsng/enable/002; type=text; label=Address for reportings by E-Mail: If not specified the $TYPO3_CONF_VARS['BE']['warning_email_addr'] will be used
        Logging.email        	= security@domain.tld

        # cat=plugin.tx_phpidsng/enable/003; type=int; label=File Threshold: Threshold for reporting an impact to the logfile defined in Logging.path
        Impact.file_threshold   = 50

        # cat=plugin.tx_phpidsng/enable/004; type=int; label=DB Threshold: Threshold for reporting an impact to the database shown in the PHPIDS backend module
        Impact.db_threshold     = 20

        # cat=plugin.tx_phpidsng/enable/005; type=int; label=EMail Threshold: Threshold for reporting an impact by e-mail specified in Logging.email
        Impact.email_threshold  = 20

        # cat=plugin.tx_phpidsng/enable/006; type=int; label=Die Threshold: Treshold for locking the homepage to the attacker with a session_destroy() and PHP die()
        Impact.die_threshold    = 35

		# cat=plugin.tx_phpidsng/enable/007; type=int; label=max_attack_count Threshold: Log attack from IP to sys_registry if this impact is reached
		Impact.mac_threshold    = 25

		# cat=plugin.tx_phpidsng/enable/008; type=string; label= max_attack_count: if max attack count of an ip is reached: die on every request of this ip ("0" = off)
        Impact.max_attack_count    = 0

        # cat=plugin.tx_phpidsng/enable/009; type=string; label= attack_timespan: max. time after which the intrusion attempt count by ip is reset (in seconds, default: 1 day)
        Impact.attack_timespan     = 3600

		# cat=plugin.tx_phpidsng/enable/010; type=string; label=Redirect URL: If defined, attackers will be redirected to this URL before PHP die()
        Impact.redirect_url    = 

        # cat=plugin.tx_phpidsng/enable/011; type=text; label=Whitelist: Define which pages should not be protected by PHPIDS. Do this by entering the ID of the specific pages. Make a comma separated list for more then one pages
        General.whitelist	    =

        # cat=plugin.tx_phpidsng/enable/012; type=text; label=GET Exceptions: Define which fields shouldn't be monitored. You can also make a comma separated list
        General.exceptions_get    = 

        # cat=plugin.tx_phpidsng/enable/013; type=text; label=POST Exceptions: Define which fields shouldn't be monitored. You can also make a comma separated list
        General.exceptions_post    =

        # cat=plugin.tx_phpidsng/enable/014; type=text; label=COOKIE Exceptions: Define which fields shouldn't be monitored. You can also make a comma separated list
        General.exceptions_cookie    = __utmz,__utmc,/.*_pk_ref.*/i,/.*mp_.*_mixpanel/i

		# cat=plugin.tx_phpidsng/enable/015; type=text; label=Scope Exceptions: Define which scopes shouldn't be monitored. You can also make a comma separated list
		General.exceptions_scope    = request

        # cat=plugin.tx_phpidsng/enable/016; type=text; label=HTML: Define which fields contain html and need preparation before hitting the PHPIDS rules
        General.html          	= __wysiwyg

        # cat=plugin.tx_phpidsng/enable/017; type=text; label=JSON: Define which fields contain JSON data and should be treated as such for fewer false positives
        General.json          	= __jsondata

        # cat=plugin.tx_phpidsng/file/001; type=text; label=HTML Purifier Path: In case you want to use a different HTMLPurifier source, specify it here. By default, those files are used that are being shipped with PHPIDS
    	General.HTML_Purifier_Path	= IDS/vendors/htmlpurifier/HTMLPurifier.auto.php

        # cat=plugin.tx_phpidsng/file/002; type=text; label=HTML Purifier Cache: In case you want to use a different HTMLPurifier cache, specify it here. By default, those files are used that are being shipped with PHPIDS
    	General.HTML_Purifier_Cache = IDS/vendors/htmlpurifier/HTMLPurifier/DefinitionCache/Serializer

        # cat=plugin.tx_phpidsng//001; type=text; label=Filter Typ: Choose your filter type. Default is XML
        General.filter_type     = xml

        # cat=plugin.tx_phpidsng//002; type=boolean; label=Scan Key: Use scan key. Default is false
        General.scan_keys       = 0

        # cat=plugin.tx_phpidsng//003; type=boolean; label=Logging Safemofe: For email logging. Default is false
        Logging.safemode        = 0

        # cat=plugin.tx_phpidsng//004; type=boolean; label=Logging urlencode: For email logging. Default is true
        Logging.urlencode       = 1

        # cat=plugin.tx_phpidsng//005; type=int; label=Logging Allowed rate: For email logging. Default is 15
        Logging.allowed_rate    = 15
		
		# cat=plugin.tx_phpidsng//006; type=string; label=Sender name: For email logging. Default is PHPIDS
        Logging.sender_name    = PHPIDS

		# cat=plugin.tx_phpidsng//007; type=string; label=Sender address: For email logging. Default is PHPIDS
        Logging.sender_address    = noreply@domain.org

        # cat=plugin.tx_phpidsng//008; type=options[file,database]; label=Caching method: Default is file
        Caching.caching         = file

        # cat=plugin.tx_phpidsng//009; type=int; label=Caching expiration time: Default is 600
        Caching.expiration_time = 600
		
	}
}

module.tx_phpidsng {
	view {
		# cat=module.tx_phpidsng/file; type=string; label=Path to template root (BE)
		templateRootPath = EXT:phpidsng/Resources/Private/Backend/Templates/
		# cat=module.tx_phpidsng/file; type=string; label=Path to template partials (BE)
		partialRootPath = EXT:phpidsng/Resources/Private/Backend/Partials/
		# cat=module.tx_phpidsng/file; type=string; label=Path to template layouts (BE)
		layoutRootPath = EXT:phpidsng/Resources/Private/Backend/Layouts/
	}
	persistence {
		# cat=module.tx_phpidsng//a; type=string; label=Default storage PID
		storagePid =
	}
}