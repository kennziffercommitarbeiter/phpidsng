<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

Tx_Extbase_Utility_Extension::registerPlugin(
	$_EXTKEY,
	'Phpidslibfe',
	'PHPIDS Lib FE'
);

if (TYPO3_MODE === 'BE') {

	/**
	 * Registers a Backend Module
	 */
	Tx_Extbase_Utility_Extension::registerModule(
		$_EXTKEY,
		'tools',	 // Make module a submodule of 'tools'
		'phpidsbe',	// Submodule key
		'',						// Position
		array(
			'PHPIDS' => 'list, truncateIntrusionTable',
		),
		array(
			'access' => 'user,group',
			'icon'   => 'EXT:' . $_EXTKEY . '/ext_icon.gif',
			'labels' => 'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_phpidsbe.xml',
		)
	);

}

t3lib_extMgm::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'PHPIDS nextGen');

t3lib_extMgm::addLLrefForTCAdescr('tx_phpidsng_domain_model_phpids', 'EXT:phpidsng/Resources/Private/Language/locallang_csh_tx_phpidsng_domain_model_phpids.xml');
t3lib_extMgm::allowTableOnStandardPages('tx_phpidsng_domain_model_phpids');
$TCA['tx_phpidsng_domain_model_phpids'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:phpidsng/Resources/Private/Language/locallang_db.xml:tx_phpidsng_domain_model_phpids',
		'label' => 'uid',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'sortby' => 'sorting',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,
		'origUid' => 't3_origuid',
		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => '',
		'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/PHPIDS.php',
		'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_phpidsng_domain_model_phpids.gif'
	),
);

?>