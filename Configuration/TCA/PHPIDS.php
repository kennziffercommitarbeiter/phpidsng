<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$TCA['tx_phpidsng_domain_model_phpids'] = array(
	'ctrl' => $TCA['tx_phpidsng_domain_model_phpids']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1,--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access,starttime, endtime'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_phpidsng_domain_model_phpids',
				'foreign_table_where' => 'AND tx_phpidsng_domain_model_phpids.pid=###CURRENT_PID### AND tx_phpidsng_domain_model_phpids.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
	),
);

$TCA['tx_phpidsng_intrusions'] = array (
	'ctrl' => $TCA['tx_pxphpids_log']['ctrl'],
	'interface' => array (
		'showRecordFieldList' => 'name,value,page,tags,ip,ip2,impact,origin,created,domain'
	),
	'feInterface' => $TCA['tx_pxphpids_log']['feInterface'],
	'columns' => array (
		'name' => array (
			'exclude' => 1,
			'label' => 'LLL:EXT:phpidsng/Resources/Private/Language/locallang_db.xlf:INTRUSION.name',
			'config' => array (
				'type' => 'input',
				'size' => '30',
			)
		),
		'value' => array (
			'exclude' => 1,
			'label' => 'LLL:EXT:phpidsng/Resources/Private/Language/locallang_db.xlf:INTRUSION.value',
			'config' => array (
				'type' => 'text',
				'cols' => '30',
				'rows' => '5',
			)
		),
		'page' => array (
			'exclude' => 1,
			'label' => 'LLL:EXT:phpidsng/Resources/Private/Language/locallang_db.xlf:INTRUSION.page',
			'config' => array (
				'type' => 'text',
				'cols' => '30',
				'rows' => '5',
			)
		),
		'tags' => array (
			'exclude' => 1,
			'label' => 'LLL:EXT:phpidsng/Resources/Private/Language/locallang_db.xlf:INTRUSION.tags',
			'config' => array (
				'type' => 'text',
				'cols' => '30',
				'rows' => '5',
			)
		),
		'ip' => array (
			'exclude' => 1,
			'label' => 'LLL:EXT:phpidsng/Resources/Private/Language/locallang_db.xlf:INTRUSION.ip',
			'config' => array (
				'type' => 'input',
				'size' => '30',
			)
		),
		'ip2' => array (
			'exclude' => 1,
			'label' => 'LLL:EXT:phpidsng/Resources/Private/Language/locallang_db.xlf:INTRUSION.ip2',
			'config' => array (
				'type' => 'input',
				'size' => '30',
			)
		),
		'impact' => array (
			'exclude' => 1,
			'label' => 'LLL:EXT:phpidsng/Resources/Private/Language/locallang_db.xlf:INTRUSION.impact',
			'config' => array (
				'type'     => 'input',
				'size'     => '4',
				'max'      => '4',
				'eval'     => 'int',
				'checkbox' => '0',
				'range'    => array (
					'upper' => '1000',
					'lower' => '10'
				),
				'default' => 0
			)
		),
		'origin' => array (
			'exclude' => 1,
			'label' => 'LLL:EXT:phpidsng/Resources/Private/Language/locallang_db.xlf:INTRUSION.origin',
			'config' => array (
				'type' => 'input',
				'size' => '30',
			)
		),
		'created' => array (
			'exclude' => 1,
			'label' => 'LLL:EXT:phpidsng/Resources/Private/Language/locallang_db.xlf:INTRUSION.created',
			'config' => array (
				'type' => 'input',
				'size' => '30',
			)
		),
		'domain' => array (
			'exclude' => 1,
			'label' => 'LLL:EXT:phpidsng/Resources/Private/Language/locallang_db.xlf:INTRUSION.domain',
			'config' => array (
				'type' => 'input',
				'size' => '30',
			)
		),
	),
	'types' => array (
		'0' => array('showitem' => 'name;;;;1-1-1, value, page, tags, ip, ip2, impact, origin, created, domain')
	),
	'palettes' => array (
		'1' => array('showitem' => '')
	)
);

?>