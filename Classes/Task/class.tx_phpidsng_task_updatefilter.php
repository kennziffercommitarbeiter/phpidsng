<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 www.kennziffer.com GmbH <info@kennziffer.com>, www.kennziffer.com GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package phpidsng
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class tx_phpidsng_task_updatefilter extends tx_scheduler_Task {
	public function execute() {
		$tmpFilePath = t3lib_extMgm::extPath('phpidsng') . 'Classes/IDS/tmp/';
		$tmpFile = $tmpFilePath . 'default_filter.xml';
		$originalXmlFilterPath = t3lib_extMgm::extPath('phpidsng') . 'Classes/IDS/';
		$originalXmlFilterFile = t3lib_extMgm::extPath('phpidsng') . 'Classes/IDS/default_filter.xml';
		$externalFileResource = 'https://dev.itratos.de/projects/php-ids/repository/raw/trunk/lib/IDS/default_filter.xml';

		try {
			if(file_put_contents($tmpFile, fopen($externalFileResource, 'r')) === FALSE) return false;

			if(file_exists($tmpFile)) {
				if(file_exists($originalXmlFilterPath . 'default_filter.xml.old')) {
					if(!unlink($originalXmlFilterPath . 'default_filter.xml.old')) return false;
				}

				if(file_exists($originalXmlFilterFile)) {
					if(!copy($originalXmlFilterFile, $originalXmlFilterPath . 'default_filter.xml.old')) return false;
				} else {
					return false;
				}

				if(file_exists($originalXmlFilterFile)) {
					if(!unlink($originalXmlFilterFile)) return false;
				} else {
					return false;
				}

				if(file_exists($tmpFilePath . 'default_filter.cache')) {
					if(!unlink($tmpFilePath . 'default_filter.cache')) return false;
				}

				if(!copy($tmpFile, $originalXmlFilterPath . 'default_filter.xml')) return false;
				if(!unlink($tmpFile)) return false;
			}
		} catch(Exception $taskException) {
			return false;
		}

		return true;
	}
}
?>