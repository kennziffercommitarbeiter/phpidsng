<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 www.kennziffer.com GmbH <info@kennziffer.com>, www.kennziffer.com GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package phpidsng
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Tx_Phpidsng_Domain_Model_PHPIDS extends Tx_Extbase_DomainObject_AbstractEntity {
	/**
	 * name
	 *
	 * @var string
	 */
	protected $name;

	/**
	 * value
	 *
	 * @var string
	 */
	protected $value;

	/**
	 * page
	 *
	 * @var string
	 */
	protected $page;

	/**
	 * tags
	 *
	 * @var string
	 */
	protected $tags;

	/**
	 * ip
	 *
	 * @var string
	 */
	protected $ip;

	/**
	 * ip2
	 *
	 * @var string
	 */
	protected $ip2;

	/**
	 * impact
	 *
	 * @var int
	 */
	protected $impact;

	/**
	 * origin
	 *
	 * @var string
	 */
	protected $origin;

	/**
	 * created
	 *
	 * @var string
	 */
	protected $created;
	
	/**
	 * domain
	 *
	 * @var string
	 */
	protected $domain;
	

	/**
	 * Returns the name
	 *
	 * @return string $name
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Sets the name
	 *
	 * @param string $name
	 * @return void
	 */
	public function setName($name = '') {
		$this->name = $name;
	}

	/**
	 * Returns the value
	 *
	 * @return string $value
	 */
	public function getValue() {
		return $this->value;
	}

	/**
	 * Sets the value
	 *
	 * @param string $value
	 * @return void
	 */
	public function setValue($value = '') {
		$this->value = $value;
	}

	/**
	 * Returns the page
	 *
	 * @return string $page
	 */
	public function getPage() {
		return $this->page;
	}

	/**
	 * Sets the page
	 *
	 * @param string $page
	 * @return void
	 */
	public function setPage($page = '') {
		$this->page = $page;
	}

	/**
	 * Returns the tags
	 *
	 * @return string $tags
	 */
	public function getTags() {
		return $this->tags;
	}

	/**
	 * Sets the tags
	 *
	 * @param string $tags
	 * @return void
	 */
	public function setTags($tags = '') {
		$this->tags = $tags;
	}

	/**
	 * Returns the ip
	 *
	 * @return string $ip
	 */
	public function getIp() {
		return $this->ip;
	}

	/**
	 * Sets the ip
	 *
	 * @param string $ip
	 * @return void
	 */
	public function setIp($ip = '') {
		$this->ip = $ip;
	}

	/**
	 * Returns the ip2
	 *
	 * @return string $ip2
	 */
	public function getIp2() {
		return $this->ip2;
	}

	/**
	 * Sets the ip2
	 *
	 * @param string $ip2
	 * @return void
	 */
	public function setIp2($ip2 = '') {
		$this->ip2 = $ip2;
	}

	/**
	 * Returns the impact
	 *
	 * @return int $impact
	 */
	public function getImpact() {
		return $this->impact;
	}

	/**
	 * Sets the impact
	 *
	 * @param int $impact
	 * @return void
	 */
	public function setImpact($impact = 0) {
		$this->impact = $impact;
	}

	/**
	 * Returns the origin
	 *
	 * @return string $origin
	 */
	public function getOrigin() {
		return $this->origin;
	}

	/**
	 * Sets the origin
	 *
	 * @param string $origin
	 * @return void
	 */
	public function setOrigin($origin = '') {
		$this->origin = $origin;
	}

	/**
	 * Returns the creation date
	 *
	 * @return string $created
	 */
	public function getCreated() {
		return $this->created;
	}

	/**
	 * Sets the created date
	 *
	 * @param string $created
	 * @return void
	 */
	public function setCreated($created = '') {
		$this->created = $created;
	}
	
	/**
	 * Returns the domain
	 *
	 * @return string $domain
	 */
	public function getDomain() {
		return $this->domain;
	}

	/**
	 * Sets the domain
	 *
	 * @param string $domain
	 * @return void
	 */
	public function setDomain($domain = '') {
		$this->domain = $domain;
	}
	
}
?>