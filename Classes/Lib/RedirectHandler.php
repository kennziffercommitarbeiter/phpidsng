<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 www.kennziffer.com GmbH <info@kennziffer.com>, www.kennziffer.com GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package phpidsng
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Tx_Phpidsng_Lib_RedirectHandler implements t3lib_Singleton {
	/**
	 * contains registry object
	 *
	 * @var t3lib_Registry
	 */
	protected $t3Registry;

	/**
	 * object manager
	 *
	 * @var Tx_Extbase_Object_ObjectManager
	 */
	protected $objectManager;

	/**
	 * registry namespace
	 *
	 * @var string
	 */
	protected $registryNamespace;

	/**
	 * inject object manager and create instance of t3lib_Registry
	 * - must be called externally BEFORE calling "logIncidentToRegistry()"
	 *
	 * @param Tx_Extbase_Object_ObjectManager $objectManager
	 * @return void
	 */
	public function injectObjectManager(Tx_Extbase_Object_ObjectManager $objectManager) {
		$this->objectManager = $objectManager;
		$this->t3Registry = $this->objectManager->get('t3lib_Registry');
	}

	/**
	 * set extensionKey ("namespace") for t3lib_Registry entry
	 *
	 * @param Tx_Extbase_Object_ObjectManager $objectManager
	 * @return void
	 */
	public function setRegistryNamespace($registryNamespace = '') {
		if(!strlen($registryNamespace)) throw new Tx_Extbase_Exception('ExtensionKey empty.');

		$this->registryNamespace = t3lib_div::camelCaseToLowerCaseUnderscored($registryNamespace);
	}

	/**
	 * - logs incident to TYPO3 CMS Registry and calculate count of intrusions per IP
	 * - reset intrusion attempts of an IP if configured timespan exceeded
	 *
	 * @param array $incident
	 * @param int $timeSpan
	 *
	 * @return void
	 */
	public function logIncidentToRegistry($incident = array(), $timeSpan = 86400) {
		//no extKey given
		if(empty($this->registryNamespace))
			throw new Tx_Extbase_Exception('Please provide a namespace first, use: setRegistryNamespace()');

		//incident must be type array and may not be empty
		if(!is_array($incident) || !count($incident))
			throw new Tx_Extbase_Exception('Incident must be of type array and contain one element');

		$ip = $incident['ip'];
		$timestamp = date('U');

		//get array of all incidents from Registry
		$incidents = $this->getIncidents();

		//increase intrusion attempt by IP - if IP in incidents array, else create new entry
		if(array_key_exists($ip, $incidents)) {
			$checkTstamp = $incidents[$ip]['tstamp'] + $timeSpan;

			//reset timestamp - timespan of last intrusion attempt of ip exceeds config value
			if($checkTstamp <= $timestamp) {
				$incidents[$ip]['attempts'] = 0;
			}

			//increase intrusion attempts of given IP
			$attempts = intval($incidents[$ip]['attempts']);
			$incidents[$ip]['tstamp'] = $timestamp;
			$incidents[$ip]['attempts'] = ++$attempts;
		} else {
			//create new IP array element and set defaults
			$incidents[$ip]['tstamp'] = $timestamp;
			$incidents[$ip]['attempts'] = 1;
		}

		//save updated intrusion attempt array back to Registry - serialized
		$this->t3Registry->set($this->registryNamespace, 'intrusionAttempts', serialize($incidents));
	}

	/**
	 * gets count of intrusion attempts of an IP from TYPO3 CMS Registry
	 *
	 * @param string $ip
	 * @return int
	 */
	public function getIntrusionAttemptCountByIp($ip) {
		$incidents = $this->getIncidents();

		if(array_key_exists($ip, $incidents)) {
			if(!is_array($incidents[$ip]) || !count($incidents[$ip]))
				throw new Tx_Extbase_Exception('There seems to be an error in saving intrusions by IP to registry');

			//return count of attempts by given IP
			return intval($incidents[$ip]['attempts']);
		}

		//IP not in array of intrusion attempts - return zero
		return 0;
	}

	/**
	 * gets incidents string from TYPO3 CMS Registry and unserialize it to incidents array
	 *
	 * @param string $ip
	 * @return int
	 */
	protected function getIncidents() {
		//return empty array if no incidents logged in Registry
		$incidents = array();

		//get intrusion attempts from Registry
		$incidentRegistryStorage = $this->t3Registry->get($this->registryNamespace, 'intrusionAttempts');

		if(!is_array($incidentRegistryStorage) && $incidentRegistryStorage != null) {
			//unserialize if serialized intrusion attempt array not empty
			$incidents =  unserialize($incidentRegistryStorage);
		}

		return $incidents;
	}
}
?>