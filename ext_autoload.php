<?php

$extensionPath = t3lib_extMgm::extPath('phpidsng');

return array(
	'tx_phpidsng_task_clearregistry' => $extensionPath . 'Classes/Task/class.tx_phpidsng_task_clearregistry.php',
	'tx_phpidsng_task_updatefilter' => $extensionPath . 'Classes/Task/class.tx_phpidsng_task_updatefilter.php'
);

?>