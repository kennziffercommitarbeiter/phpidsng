<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 www.kennziffer.com GmbH <info@kennziffer.com>, www.kennziffer.com GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package phpidsng
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Tx_Phpidsng_Controller_PHPIDSController extends Tx_Extbase_MVC_Controller_ActionController {

	/**
	 * pHPIDSRepository
	 *
	 * @var Tx_Phpidsng_Domain_Repository_PHPIDSRepository
	 *
	 */
	protected $pHPIDSRepository;

	/**
	 * redirect handling
	 *
	 * @var Tx_Phpidsng_Lib_RedirectHandler
	 *
	 */
	protected $redirectHandler;

	/**
	 * debug mode
	 *
	 * @var boolean
	 *
	 */
	protected $debugMode;

	/**
	 * @param Tx_Phpidsng_Domain_Repository_PHPIDSRepository $pHPIDSRepository
	 */
	public function injectLoggingService(Tx_Phpidsng_Domain_Repository_PHPIDSRepository $pHPIDSRepository) {
		$this->pHPIDSRepository = $pHPIDSRepository;
	}

	/**
	 * action list
	 *
	 * @return void
	 */
	public function listAction() {
			$pHPIDs = $this->pHPIDSRepository->findAll();
			$this->view->assign('pHPIDs', $pHPIDs);
	}

	/**
	 * action truncateIntrusionTable
	 * - truncates intrusions table
	 *
	 * @return void
	 */
	public function truncateIntrusionTableAction() {
		$res = $GLOBALS['TYPO3_DB']->sql_query('TRUNCATE tx_phpidsng_intrusions');
		$this->redirect('list');
	}

	protected function initialize() {
		require_once(t3lib_extMgm::extPath('phpidsng').'Classes/IDS/Log/Interface.php');
		require_once(t3lib_extMgm::extPath('phpidsng').'Classes/IDS/Log/File.php');
		require_once(t3lib_extMgm::extPath('phpidsng').'Classes/IDS/Log/Composite.php');
		require_once(t3lib_extMgm::extPath('phpidsng').'Classes/IDS/Log/Email.php');
		require_once(t3lib_extMgm::extPath('phpidsng').'Classes/IDS/Log/Database.php');
		require_once(t3lib_extMgm::extPath('phpidsng').'Classes/IDS/Monitor.php');
		require_once(t3lib_extMgm::extPath('phpidsng').'Classes/IDS/Filter/Storage.php');
		require_once(t3lib_extMgm::extPath('phpidsng').'Classes/IDS/Caching/Factory.php');
		require_once(t3lib_extMgm::extPath('phpidsng').'Classes/IDS/Caching/Interface.php');
		require_once(t3lib_extMgm::extPath('phpidsng').'Classes/IDS/Filter.php');
		require_once(t3lib_extMgm::extPath('phpidsng').'Classes/IDS/Report.php');
		require_once(t3lib_extMgm::extPath('phpidsng').'Classes/IDS/Converter.php');
		require_once(t3lib_extMgm::extPath('phpidsng').'Classes/IDS/Event.php');
		require_once(t3lib_extMgm::extPath('phpidsng').'Classes/IDS/Init.php');

		$this->redirectHandler = $this->objectManager->get('Tx_Phpidsng_Lib_RedirectHandler');
		$this->redirectHandler->setRegistryNamespace('tx_phpidsng');
		$this->debugMode = (bool)$this->settings['General']['debug_mode'];
	}

	/**
	 * set configuration data for phpids initialization
	 *
	 * - apply typoscript configuration to ids initial config
	 * - apply non-changeable settings
	 *
	 * @return IDS_Init
	 */
	protected function setConfigData() {
		//TODO: Optimize this method - it's quite ugly
		$phpidsBasePath = t3lib_extMgm::extPath('phpidsng'). 'Classes/IDS/';

		//init basic IDS configuration object
		$initObject = IDS_Init::init();

		//general settings: apply user defined config from typoscript
		$initObject->config['General'] = $this->settings['General'];

		//apply non-changeable values
		$initObject->config['General']['base_path'] = $phpidsBasePath;
        $initObject->config['General']['use_base_path'] = true;
		$initObject->config['General']['filter_path'] = 'default_filter.xml';
		$initObject->config['General']['tmp_path'] = 'tmp/';
		$initObject->config['General']['min_php_version'] = '5.1.6';

		// merge exception configurations
		$initObject = Tx_Phpidsng_Lib_Utility::mergeExceptionConfig('GET', $this->settings['General']['exceptions_get'], $initObject);
		$initObject = Tx_Phpidsng_Lib_Utility::mergeExceptionConfig('POST', $this->settings['General']['exceptions_post'], $initObject);
		$initObject = Tx_Phpidsng_Lib_Utility::mergeExceptionConfig('COOKIE', $this->settings['General']['exceptions_cookie'], $initObject);

		//logging: apply user defined config from typoscript
		$logEmail = empty($this->settings['Logging']['email']) ?
					$GLOBALS['TYPO3_CONF_VARS']['BE']['warning_email_addr'] :
					$this->settings['Logging']['email'];

		$initObject->config['Logging'] = $this->settings['Logging'];

		//apply non-changeable settings
		$initObject->config['Logging']['allowed_rate'] = '500';
		$initObject->config['Logging']['path'] = 'tmp/phpids_log.txt';
		$initObject->config['Logging']['envelope'] = '';
		$initObject->config['Logging']['recipients'] = $logEmail;
		$initObject->config['Logging']['subject'] = 'PHPIDS detected an intrusion attempt on ' . $_SERVER['SERVER_NAME'];

		// build and setemail header
		$emailHeader = 'From: ';
		$emailHeader .= $this->settings['Logging']['sender_name'];
		$emailHeader .= ' <'.$this->settings['Logging']['sender_address'].'>';
		$initObject->config['Logging']['header'] = $emailHeader;

        //database logging: apply database connection settings from TYPO3 CMS internal database
		$db_port = '3306';
		if(stristr(TYPO3_db_host, ':')) {
			list($host, $db_port) = t3lib_div::trimExplode(':', TYPO3_db_host);
		}

		$dbConnectionString =   'mysql:host=' . TYPO3_db_host .
								';port=' . $db_port .
								';dbname=' . TYPO3_db;

		$initObject->config['Logging']['wrapper'] = $dbConnectionString;
		$initObject->config['Logging']['user'] = TYPO3_db_username;
		$initObject->config['Logging']['password'] = TYPO3_db_password;
		$initObject->config['Logging']['table'] = 'tx_phpidsng_intrusions';

		//apply genering caching configuration from typoscript
		$initObject->config['Caching'] = $this->settings['Caching'];

		//set non-changeable caching settings
		$initObject->config['Caching']['path'] = 'tmp/default_filter.cache';
		$initObject->config['Caching']['wrapper'] = $dbConnectionString;
		$initObject->config['Caching']['user'] = TYPO3_db_username;
		$initObject->config['Caching']['password'] = TYPO3_db_password;
		$initObject->config['Caching']['table'] = 'tx_phpidsng_cache';

		return $initObject;
	}

	/*
	 * action protect
	 * included by typoscript, handles frontend incidents
	 *
	 * @return void
	 */
	protected function protectAction() {
		if (!session_id()) {
			session_start();
		}

		//basic settings for use of PHPIDS by this extension
		$this->initialize();
		$init = $this->setConfigData();

		//ip already blocked? stop processing immediately
		if($this->checkIfCurrentIpAlreadyBlocked() === true) {
			session_destroy();
			die();
		}

		// check if current pid is whitelisted
		$whitelisted = Tx_Phpidsng_Lib_Utility::checkWhitelist($this->settings['General']['whitelist']);

		//check if ip has access to debug mode
		$ipAccess = Tx_Phpidsng_Lib_Utility::checkIpRestriction();

		//debug mode
		if($this->debugMode === true && $ipAccess === true) {
			$this->view->assign('debugMode', $this->debugMode);
			$this->view->assign('currentPage', $GLOBALS['TSFE']->id);
			$this->view->assign('phpidsConfig', $this->settings);
			$this->view->assign('whitelisted', $whitelisted);
		}

		// stop execution when current page is whitelisted
		if ($whitelisted) return;

		try {
			//do IDS stuff here
			$this->processIdsRequest($init);
		} catch(Exception $idsException) {
			if($this->debugMode) {
				$this->flashMessageContainer->add(
					$idsException->getFile() . ': ' . $idsException->getMessage(),
					t3lib_FlashMessage::ERROR
				);

				$this->view->assign('phpidsResults', array());
			}
		}
	}

	/*
	 * processIdsRequest
	 * send request to IDS, evaluate and react
	 *
	 * @param $init IDS_Init
	 * @return void
	 */
	protected function processIdsRequest(IDS_Init $init) {
		//init IDS Moitoring and process current request
		$currentIpAddress = Tx_Phpidsng_Lib_Utility::getIpAddress();
		$ids = new IDS_Monitor($this->getIdsRequest(), $init);
		$result = $ids->run();

		//check result on intrusion
		if (!$result->isEmpty()) {
			if($this->debugMode && Tx_Phpidsng_Lib_Utility::checkIpRestriction() === true) {
				//details about incident
				$this->view->assign('phpidsResults', $result->getIterator());
			}

			//init IDS logging object
			$compositeLog = new IDS_Log_Composite();

			//log to file - if min. "file_threshold" reached
			if ($result->getImpact() >= $this->settings['Impact']['file_threshold']) {
				$compositeLog->addLogger(IDS_Log_File::getInstance($init));
			}

			//log to database - if min. "db_threshold" reached
			if ($result->getImpact() >= $this->settings['Impact']['db_threshold']) {
				$compositeLog->addLogger(IDS_Log_Database::getInstance($init));
			}

			//send mail - if min. "email_threshold" reached
			if ($result->getImpact() >= $this->settings['Impact']['email_threshold']) {
				$compositeLog->addLogger(IDS_Log_Email::getInstance($init));
			}

			//execute log actions
			$compositeLog->execute($result);

			if(
				$this->settings['Impact']['max_attack_count'] > 0 &&
				$result->getImpact() >= $this->settings['Impact']['mac_threshold']
			) {
				$this->redirectHandler->logIncidentToRegistry(
					array('ip' => $currentIpAddress),
					$this->settings['Impact']['attack_timespan']
				);
			}

			//min. die threshold reached: stop working immediately!
			if ($result->getImpact() >= $this->settings['Impact']['die_threshold'] ||
				$this->checkIfCurrentIpAlreadyBlocked()
			) {
				session_destroy();

				// redirect user to another url if set in config
				if (!empty($this->settings['Impact']['redirect_url'])) {
					header('Location: '.$this->settings['Impact']['redirect_url']);
				}

				die();
			}
		}
	}

	/*
	 * checks if current ip already exists on registry blocking list
	 *
	 * @return boolean
	 */
	protected function checkIfCurrentIpAlreadyBlocked() {
		//if IP already reached blocking limit - block immediately
		if (
			$this->redirectHandler->getIntrusionAttemptCountByIp(Tx_Phpidsng_Lib_Utility::getIpAddress()) >=
			$this->settings['Impact']['max_attack_count'] &&
			$this->settings['Impact']['max_attack_count'] > 0
		) {
			return true;
		}

		return false;
	}

	/*
	 * IDS requires scopes to be inspected - return scopes here
	 *
	 * @return array
	 */
	protected function getIdsRequest() {
		//list of scopes to ignore, taken from configuration
		$excludeScopeList = $this->settings['General']['exceptions_scope'];

		$requestScopes = array(
			'REQUEST' => $_REQUEST,
			'GET' => $_GET,
			'POST' => $_POST,
			'COOKIE' => $_COOKIE
		);

		//unset scopes excluded by configuration settings
		if(!empty($excludeScopeList)) {
			$scopeExceptions = t3lib_div::trimExplode(',', $excludeScopeList);

			if(is_array($scopeExceptions) && count($scopeExceptions)) {
				foreach($scopeExceptions as $scopeException) {
					$scopeException = strtoupper($scopeException);
					if(array_key_exists($scopeException, $requestScopes)) {
						unset($requestScopes[$scopeException]);
					}
				}
			}
		}

		return $requestScopes;
	}
}
?>