<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 www.kennziffer.com GmbH <info@kennziffer.com>, www.kennziffer.com GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package phpidsng
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Tx_Phpidsng_Lib_Utility implements t3lib_Singleton {
	/*
	* check if current page is set as whitelisted in config
	*
	* @return boolean
	*/
	static function checkWhitelist($whiteList = '') {
		$whitelistedPages = t3lib_div::trimExplode(',', $whiteList);
		if (t3lib_div::inArray($whitelistedPages, $GLOBALS['TSFE']->id)) {
			return true;
		} else return false;
	}

	/*
	 * merge exception config to ids config
	 *
	 * @param $scope		string	'GET', 'POST', 'COOKIE'
	 * @param $configValue	string
	 * @param $initObject	IDS_Init
	 * @return void
	 */
	static function mergeExceptionConfig($scope, $configValue, $initObject) {

		// end processing if no scope is set
		if (empty($scope)) return $initObject;

		// parse scope string
		$scope .=  '.';

		// explode config value
		if (!empty($configValue)) {
			$params = t3lib_div::trimExplode(',', $configValue);
		}

		// merge param with parsed scope to IDS config array
		if (is_array($params) && count($params)) {
			foreach ($params as $param) {
				$initObject->config['General']['exceptions'][] = $scope.$param;
			}
		}

		return $initObject;
	}

	/**
	 * checkIpRestriction
	 * - check current ip against [SYS][devIPmask]
	 *
	 * @return boolean
	 */
	static function checkIpRestriction() {
		$ipAccess = false;

		$ipAccess = t3lib_div::cmpIP(
			self::getIpAddress(),
			$GLOBALS['TYPO3_CONF_VARS']['SYS']['devIPmask']
		);

		return $ipAccess;
	}

	/**
	 * get ip address
	 * - gets current ip address
	 *
	 * @return boolean
	 */
	static function getIpAddress() {
		$currentIpAddress = (!isset($_SERVER['HTTP_X_FORWARDED_FOR']))?
			$_SERVER['REMOTE_ADDR'] :
			$_SERVER['HTTP_X_FORWARDED_FOR'];

		return $currentIpAddress;
	}
}