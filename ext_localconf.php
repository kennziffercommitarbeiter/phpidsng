<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks']['tx_phpidsng_task_clearregistry'] = array(
	'extension'        => $_EXTKEY,
	'title'            => 'PHPIDS nextGen: Clear intrusion attempt array in sys_registry',
	'description'      => 'PHPIDS nextGen: Clear intrusion attempt array in sys_registry.'
);

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks']['tx_phpidsng_task_updatefilter'] = array(
	'extension'        => $_EXTKEY,
	'title'            => 'PHPIDS nextGen: Update default_filter.xml file.',
	'description'      => 'PHPIDS nextGen: Update default_filter.xml file.'
);

//add pageTsConfig from file
t3lib_extMgm::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:phpidsng/Configuration/TypoScript/pageTSconfig.txt">');

Tx_Extbase_Utility_Extension::configurePlugin(
	$_EXTKEY,
	'Phpidslibfe',
	array(
		'PHPIDS' => 'protect',

	),
	// non-cacheable actions
	array(
		'PHPIDS' => 'protect',

	)
);

?>